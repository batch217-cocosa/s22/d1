/*

	Mutator methods are functions that mutate



	these merhods manipulater the original array performing various taasls suvh as adding and removing elements



*/


let fruits = ["Apple", 'Orange', 'Kiwi', 'Gragon Fruit'];


console.log("Current array: ");
console.log(fruits);

// push(); method





let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Using push method")
console.log(fruits);


// Adding multiple elements/ value to ab array

fruits.push("avocado", "guava");

console.log("Using push method")
console.log(fruits);

// pop();


let removedFruit = fruits.pop();

console.log(removedFruit);

console.log("Mutated array after pop(): ");
console.log(fruits);





// unshift();

console.log("Mutated array from unshift");

fruits.unshift("Lime", "Banana");

console.log(fruits);


// shift();


let anotherFruit = fruits.shift();

console.log(anotherFruit);

console.log("Mutated array from shift method");


console.log(fruits);




// splice();


// arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);


fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);


// sort();


fruits.sort();


console.log(fruits);


// reverse();


fruits.reverse();

console.log(fruits);





// Non-Mutator Methods

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE" ];

// indexOf();

// arrayName.indexOf(searchValue, fromIndex);

let firstIndex = countries.indexOf ('PH');

console.log(firstIndex);

let invalidCountry = countries.indexOf("BR");

console.log(invalidCountry);


// lastIndexOf()

// getting the index number starting from the last element


let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 6);

console.log(lastIndexStart);

// slice();


// arrayName.slice(startingIndex);

// arrayName.slice(startingIndex,endingIndex);



let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);


let slicedArrayB = countries.slice(2,4);
console.log(slicedArrayB);

// slicing from the last element


let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);



// toString();


console.log(countries.toString());



// concat();

// arrayA.concat(arrayB);
// arrayA.concat(elementA);

let tasksArrayA = ["drink html", "eat javascript"];
let tasksArrayB = ["inhale css", "breath sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log(tasks);



// Combining multiple arrays

console.log("result from concat method: ");

let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);


console.log(allTasks);


// combining arrays with elements

let combinedTasks = tasksArrayA.concat("smell express", "throw react");

console.log("Result from concat method");
console.log(combinedTasks);



// join();

// arrayName.join("separatorString");



let users = ["John", "Jane", "Joe", "Robert"];

console.log(users.join());

console.log(users.join(""));

console.log(users.join(' - '));

console.log(users.join(' | '));



// Iteration Methods

// forEach();

// arrayName.forEach(function(indivElement)){statement}


allTasks.forEach(function(tasks){

	console.log(tasks);

});


let filteredTasks = [];

// looping through all array items


allTasks.forEach(function(tasks){

	// if the element/strings length is greater than 10 characters

	if(tasks.length > 10){
		filteredTasks.push(tasks);
	}

});


console.log(filteredTasks);



// map();


// resultArray.map(function(indivElement));


let numbers = [1, 2, 3, 4, 5];


let numberMap = numbers.map(function(numbers){
	return numbers * numbers;
});


console.log("Original Array: ");
console.log(numbers); // original is unaffected by map
console.log("Result of map method: ")
console.log(numberMap);



// map() vs forEach();


let numberForEach = numbers.forEach(function(numbers){
	return numbers * numbers;
});


console.log(numberForEach); // undefined result



// every()

/*

resultArray = arrayName.every(function(){

return expression/condition;

})

*/


let allValid = numbers.every(function(numbers){

	return ( numbers < 0 );
})

console.log("result of every method: ");

console.log(allValid);


// some()


let someValid = numbers.some(function(numbers){
	return (numbers < 3);
})

console.log(someValid);


if(someValid){

	console.log("Some numbers in the array are greater than 2");

};


// filter()


/*

resultArray = arrayName.filter(function(indivElement){
	
	return expression/condition

});

*/




let filterValid = numbers.filter(function(numbers){
	return (numbers < 3);
});


console.log("Result from filter method");

console.log(filterValid);


// no element found

let nothingFound = numbers.filter(function(numbers){
	return (numbers = 0);
})

console.log(nothingFound); //it will return empty brackets



// filtering using forEach

let filteredNumbers = [];


numbers.forEach(function(numbers){
	if (numbers < 3){
		filteredNumbers.push(numbers);
	}
});

console.log(filteredNumbers);



// includes()


// arrayName.includes(<arguments>)


let products = ["Mouse","Keyboard","Laptop","Monitor"];


let productsFound = products.includes("Mouse");
console.log(productsFound);


let productsFound1 = products.includes("Headset");
console.log(productsFound1);

//reduce()


/*

Syntax --->   let/const resultArray = arrayName.reduce(function(accumulator,currentValue){
	return expression/condition;

})

*/


let iteration = 0;

let reduceArray = numbers.reduce(function(x, y){

	console.warn("current iteration : " + ++iteration );
	console.log("accumulator: " + x);
	console.log("currentValue: " + y);


	return x + y;

});


console.log("Result of reduce method:" + reduceArray);